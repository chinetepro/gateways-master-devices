const development = {
  saltWorkFactor: 10,
  refreshTokenTime: "1y",
  accessTokenTime: "30m",
  port: 1337,
  host: "localhost",
  mongoDbUrl: "mongodb://localhost:27017/gateways-master-api",
  privateKey: `-----BEGIN RSA PRIVATE KEY-----
  MIIBPAIBAAJBAMN/BJ6VTSc7zJp8lX71PYgi4oTRIkfI4keSIVAtDySB2D9mPQZJ
  akxAPSnlgyy4UMXibRbW6b2Ndd2ths+7hI8CAwEAAQJACUrBQnRgPWwnNnSTenxX
  l1/pGl0CXx0aPEKi2Le7j4WTHH29sPrtxzEiz0iDF5HPIeEn4wS9PdZsID/6n5M/
  IQIhAO8PVEoWTRyIyILMxKm4FYReaql0KYGH+3yJAq5HXkrfAiEA0VlrmHoVjcYv
  0VcvSGVbpwlYOaLKUGk1cFvPBEporFECIQDf2zHp4nI46xkf6pGp/9geSK+GmGEZ
  i3WCi2X4PppcbwIhAIGj5kVeFMre1+3DF7y1R7SAzOmkLbTt8fHKaMA3c+qBAiEA
  wSsXos8W+oUPqtCDTi5NoVOpnpLcnuedbieArXN2kZM=
  -----END RSA PRIVATE KEY-----`
};

const test = {
  ...development,
  port: 1338,
  host: "localhost",
  mongoDbUrl: "mongodb://localhost:27017/gateways-master-api-testing",
};

const config = {
  development,
  test,
};

const enviroment = process.env.NODE_ENV ? process.env.NODE_ENV : "development"; // 'dev' or 'test'
// @ts-ignore
export default config[enviroment];
