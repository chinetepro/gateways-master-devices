import { apiRequest, should } from '../testConfig';
import User, { UserDocument } from "../../src/model/user.model";

/**
 * Test cases to test all the authentication APIs
 * Covered Routes:
 * (1) Register
 * (2) Login
 * (3) Session
 * (4) Logout
 * (5) //TODO Verify Confirm OTP
 * (6) //TODO Verify Confirm OTP
 */

interface AuthData {
	accessToken: string | undefined;
	refreshToken: string | undefined;
}

// Prepare data for testing
export let testData: UserDocument | any = {
	"name": "chinetepro",
	"password": "Test@1234",
	"email": "chinetepro@gmail.com",
	'confirmOTP': ""
};

let authData: AuthData = {
	accessToken: undefined,
	refreshToken: undefined,
};



export const useSignup = (done: any) => {
	apiRequest
		.post("/api/users")
		.send(testData)
		.end((err, res) => {
			res.should.have.status(200);
			res.body.should.have.a("object");
			res.body.should.have.property("_id");
			testData = { ...testData, ...res?.body };
			done();
		});
};

export const useLogin = (done: any) => {
	apiRequest
			.post("/api/sessions")
			.send({ "email": testData.email, "password": testData.password })
			.end((err, res: any) => {
				res.should.have.status(200);
				res.body.should.have.property("accessToken");
				res.body.should.have.property("refreshToken");
				const { accessToken, refreshToken } = res.body;
				authData = { accessToken: `Bearer ${accessToken}`, refreshToken };
				done();
			});
};

export const useRequestGetAuth = (route: string) => {
	return apiRequest
		.get(route)
		.set('authorization', `${authData.accessToken}`)
		.set('x-refresh', `${authData.refreshToken}`);
};

export const useRequestPostAuth = (route: string) => {
	return apiRequest
		.post(route)
		.set('authorization', `${authData.accessToken}`)
		.set('x-refresh', `${authData.refreshToken}`);
};

export const useRequestPutAuth = (route: string,) => {
	return apiRequest
		.put(route)
		.set('authorization', `${authData.accessToken}`)
		.set('x-refresh', `${authData.refreshToken}`);
};

export const useRequestPatchAuth = (route: string) => {
	return apiRequest
		.patch(route)
		.set('authorization', `${authData.accessToken}`)
		.set('x-refresh', `${authData.refreshToken}`);
};

export const useRequestDeleteAuth = (route: string) => {
	return apiRequest
		.delete(route)
		.set('authorization', `${authData.accessToken}`)
		.set('x-refresh', `${authData.refreshToken}`);
};

describe("Auth", () => {

	// Before each test we empty the database
	before((done) => {
		User.deleteMany({}, (err: any) => {
			done();
		});
	});

	
   /*
	* Test the /POST route
  */
	describe("/Services OK", () => {
		it("It should show 200 status", (done) => {
			apiRequest
				.get("/healthcheck")
				.end((err, res) => {
					res.should.have.status(200);
					done();
				});
		});
	});


	/*
  * Test the /POST route
  */
	describe("/POST Register", () => {
		it("It should send validation error for Register", (done) => {
			apiRequest
			.post("/api/users")
			.send({ "email": testData.email })
			.end((err, res) => {
				res.should.have.status(400);
				done();
			});
		});
	});

	/*
  * Test the /POST route
  */
	describe("/POST Register", () => {
		it("It should validate password error on Register user", (done) => {
			apiRequest
				.post("/api/users")
				.send(testData)
				.end((err, res) => {
					res.should.have.status(400);
					testData.password = 'Robert1234';
					done();
				});
		});
	});

	describe("/POST Register", () => {
		it("It should Register user", (done) => {
			useSignup(done)
		});
	});

	/*
  * Test the /POST route
  */
	describe("/POST Login", () => {
		it("it should login user", (done) => {
			useLogin(done);
		});
	});

	/*
  * Test the /GET route
  */
	describe("/GET Get the user's forbidden", () => {
		it("It should  forbidden to Get the user session", (done) => {
			apiRequest
				.get("/api/sessions")
				.send({})
				.end((err, res) => {
					res.should.have.status(403);
					res.should.have.property('error')
					res.error.should.have.property('text').eql('Forbidden');
					done();
				});
		});
	});

	describe("/GET Get the user's sessions", () => {
		it("It should Get the user session", (done) => {
			useRequestGetAuth("/api/sessions")
				.send()
				.end((err, res) => {
					res.should.have.status(200);
					done();
				});
		});
	});

	/*
  * Test the /POST route
  */
	describe("/Logout user ", () => {
		it("It should logout user session forbidden", (done) => {
			apiRequest
				.delete("/api/sessions")
				.send()
				.end((err, res) => {
					res.should.have.status(403);
					done();
				});
		});

		it("It should logout user", (done) => {
			useRequestDeleteAuth("/api/sessions")
				.send()
				.end((err, res) => {
					res.should.have.status(200);
					done();
				});
		});
	});

	describe("/Logout user is logout or no ", () => {
		it("It should user is logout or no", (done) => {
			apiRequest
				.get("/api/sessions")
				.send()
				.end((err, res) => {
					res.should.have.status(403);
					done();
				});
		});
	});

});