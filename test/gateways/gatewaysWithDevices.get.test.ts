import Gateway from "../../src/model/gateway.model";
import Device from "../../src/model/device.model";
import { useRequestGetAuth } from "../auth/auth.test";
import { beforeTestGateways } from "./gatewaysConfig";
import { should } from '../testConfig';
import mongoose from "mongoose";


describe("Gateways with devices", () => {
  beforeTestGateways();

  const gOne = {
    serialNumber: "1234",
    name: "Gateway 1",
    ipv4Address: "255.0.0.0",
  };

  const dOne = {
    vendor: "vendor 1",
    status: "offline",
  };

  const dTwo = {
    status: "online",
    vendor: "vendor 2",
  };

  beforeEach(async () => {
    //Before each test we empty the database
    const [gateway] = await Promise.all([Gateway.create(gOne), Device.deleteMany({})]);    

    // Creating two devices attached to this gateway
    await Promise.all([Device.create({ ...dOne, gateway: gateway._id }), Device.create({ ...dTwo, gateway: gateway._id })]);    
  });

  describe("/GET gateway with devices", () => {
    it("it should GET one gateway", (done) => {
      useRequestGetAuth("/api/gatewaysWithDevices")
        .end((err, res) => {
          res.body[0].should.include(gOne);
          res.body[0].devices[0].should.include(dOne);
          res.body[0].devices[1].should.include(dTwo);
          done();
        });
    });
  });
});
