import Gateway from "../../src/model/gateway.model";
import { testData, useRequestGetAuth } from "../auth/auth.test";
import { beforeTestGateways } from "./gatewaysConfig";
import { should } from '../testConfig';
import mongoose from "mongoose";

describe("Gateways Get", () => {
  beforeTestGateways();

  describe("/GET gateway no data", () => {
    it("it should GET 0 gateways", (done) => {
      useRequestGetAuth("/api/gateways")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("array");
          res.body.length.should.be.eql(0);
          done();
        });
    });
  });

  // describe("/GET gateway on object in data", () => {
  //   let gateway = {
  //     serialNumber: "4352",
  //     name: "Gateway Tests",
  //     ipv4Address: "127.0.0.0",
  //     userId: testData._id
  //   };
    
  //   beforeEach(async () => {
  //     //Before each test we empty the database
  //     beforeTestGateways();
  //     await Gateway.create(gateway);
  //   });
    
  //   it("it should GET at least one gateway", (done) => {
  //       useRequestGetAuth("/api/gateways")
  //         .end((err, res) => {
  //           res.should.have.status(200);
  //           res.body.length.should.be.eql(1);
  //           res.body[0].should.include(gateway);
  //           done();
  //         });
  //     });
  // });
});
