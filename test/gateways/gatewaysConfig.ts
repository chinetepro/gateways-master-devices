import Gateway from "../../src/model/gateway.model";
import { useLogin } from "../auth/auth.test";
import mongoose from "mongoose";

const beforeTestGateways = () => beforeEach((done) => {
  // Login use
  useLogin(done)
  //Before each test we empty the database
  Gateway.deleteMany({}, (err: any) => {});
});

export { beforeTestGateways };