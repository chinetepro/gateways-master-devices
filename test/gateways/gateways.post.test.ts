import Gateway, { GatewayDocument } from "../../src/model/gateway.model";
import { testData, useRequestPostAuth } from "../auth/auth.test";
import { beforeTestGateways } from "./gatewaysConfig";
import { expect } from "chai";
import { should } from '../testConfig';
import mongoose from "mongoose";


describe("Gateways /Post", () => {
  beforeTestGateways();

  it("it should not POST a gateway without serialNumber field", (done) => {
    const gateway = {
      name: "Gateway Tests",
      ipv4Address: "127.0.0.0",
      userId: testData._id
    };
    useRequestPostAuth("/api/gateways")
      .send(gateway)
      .end((err, res) => {
        console.log(res.body)
        res.body.path.should.equal("body.serialNumber");
        done();
      });
  });

  it("it should not POST a gateway with an invalid Ipv4 address", (done) => {
    const gateway = {
      userId: testData._id,
      serialNumber: "2534",
      name: "Gateway 1",
      ipv4Address: "1277.0.0.0",
    };
    useRequestPostAuth("/api/gateways")
      .send(gateway)
      .end((err, res) => {
        res.body.path.should.equal("body.ipv4Address");
        done();
      });
  });

  it("it should POST a gateway successfully", (done) => {
    const gateway: any = {
      name: "Gateway 1",
      serialNumber: "8234",
      ipv4Address: "127.0.0.1",
      userId: testData._id
    };
    useRequestPostAuth("/api/gateways")
      .send(gateway)
      .end(async (err, res) => {
        res.body.should.be.a("object");
        const gatewayInDb: GatewayDocument = await Gateway.findOne({ _id: res.body._id ,userId: testData._id}).lean();
        gatewayInDb.serialNumber.should.equal(gateway.serialNumber);
        done();
      });
  });
});
