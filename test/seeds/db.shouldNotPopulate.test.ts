import { expect } from "chai";
import Gateway from "../../src/model/gateway.model";
import { useRequestGetAuth } from "../auth/auth.test";
import { beforeTestGateways } from "../gateways/gatewaysConfig";

describe("Db should be Not populated", () => {
  const gateway = {
    serialNumber: "2534",
    name: "Gateway 1",
    ipv4Address: "155.0.0.0",
  };
  beforeEach((done) => {
    Gateway.create(gateway, () => {
      done();
    });
  });

  it("it should return not Forbidden", (done) => {
    useRequestGetAuth("/api/needPopulate")
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });

  describe("it should return No", () => {
    // @ts-ignore
    beforeTestGateways();
  
    it("Return No ok ", (done) => {
      useRequestGetAuth("/api/needPopulate")
      .end((err, res) => {
        res.should.have.status(200);
        expect(res.text).eq("No");
        done();
      });
    });
  });
});
