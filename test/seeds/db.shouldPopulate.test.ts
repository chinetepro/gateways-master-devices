import { expect } from "chai";
import Gateway from "../../src/model/gateway.model";
import Device from "../../src/model/device.model";
import { useRequestGetAuth } from "../auth/auth.test";
import { beforeTestGateways } from "../gateways/gatewaysConfig";

describe("Db should be populated", () => {
  beforeEach(async () => {
    //Before each test we empty the database
    await Gateway.deleteMany({});
    await Device.deleteMany({});
  });

  it("it should return not Forbidden", (done) => {
    useRequestGetAuth("/api/needPopulate")
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });

  describe("it should return Yes", () => {
    // @ts-ignore
    beforeTestGateways();
  
    it("Return Yes ok ", (done) => {
      useRequestGetAuth("/api/needPopulate")
      .end((err, res) => {
        res.should.have.status(200);
        expect(res.text).eq("Yes");
        done();
      });
    });
  });
});
