import { expect } from "chai";
import Gateway from "../../src/model/gateway.model";
import Device from "../../src/model/device.model";
import { gateways } from "../../src/seeds/gateways.seed";
import { devices } from "../../src/seeds/devices.seed";
import { useRequestPostAuth } from "../auth/auth.test";
import { beforeTestGateways } from "../gateways/gatewaysConfig";

describe("Db will be populated succesfully", () => {
  beforeEach(async () => {
    //Before each test we empty the database
    await Gateway.deleteMany({});
    await Device.deleteMany({});
  });
  
  afterEach(async () => {
    //Before each test we empty the database
    const gatewaysInDb = await Gateway.find({}, {}, { lean: true }).exec();
    const devicesInDb = await Device.find(
      {},
      {},
      { lean: true }
    ).exec();
      console.log(gatewaysInDb, devicesInDb)
    for (let g of gatewaysInDb) {
      const g2 : any = gateways.find((g2) => g2._id == g._id);
      if (g2) {
        delete g2._id;
      }
      expect(g).includes(g2);
    }

    for (let p of devicesInDb) {
      const p2 : any = devices.find((p2) => p2._id == p._id);
      if (p2) {
        delete p2._id;
        delete p2.createdAt;
        delete p2.gateway;
      }
      
      expect(p).includes(p2);
    }
    
    await Gateway.deleteMany({});
    await Device.deleteMany({});
  });
});

it("populated forbidden ", (done) => {
  useRequestPostAuth("/api/populate")
    .end((err, res) => {
      res.should.have.status(403);
      done();
    });
});

describe("it will be populated", () => {
  beforeTestGateways();

  it("populated ok ", (done) => {
  useRequestPostAuth("/api/populate")
    .end((err, res) => {
      res.should.have.status(200);
      done();
    });
  });
});
