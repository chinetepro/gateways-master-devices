//During the automated test the env variable, We will set it to "test"
process.env.NODE_ENV = "test";

//Import the dev-dependencies
import chai from "chai";
import chaiHttp from "chai-http";
import app from "../src/app";
let should = chai.should();

//Export this to use in multiple files
chai.use(chaiHttp);

const apiRequest = chai.request(app).keepOpen();

export {
	chai,
	chaiHttp,
	app,
	should,
	apiRequest
};