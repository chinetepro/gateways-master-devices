import express, { Request, Response } from "express";
import * as SessionController from "../controllers/session.controller";
import * as GatewayController from "../controllers/gateway.controller";
import * as DeviceController from "../controllers/device.controller";
import * as UserSchema from "../schema/user.schema";
import * as GatewaySchema from "../schema/gateway.schema";
import { createUserFn } from "../controllers/user.controller";
import { requiresUser, validateRequest } from "../middleware";
import * as DeviceSchema from "../schema/device.schema";
import { needPopulate, populateDB } from "../seeds/db.populate";

const routes = express.Router();

routes.get("/healthcheck", (req: Request, res: Response) => {
  res.sendStatus(200);
});

// -----------------------------Auth Routes-------------------------------------------
// Register user
routes.post(
  "/api/users",
  validateRequest(UserSchema.createUserSchema),
  createUserFn
);

// Login
routes.post(
  "/api/sessions",
  validateRequest(UserSchema.createUserSessionSchema),
  SessionController.createUserSessionFn
);

// Get the user's sessions
routes.get(
  "/api/sessions",
  requiresUser,
  SessionController.getUserSessionsFn
);

// Logout
routes.delete(
  "/api/sessions",
  requiresUser,
  SessionController.invalidateUserSessionFn
);

// -----------------------------Populate DB-------------------------------------------

routes.post("/api/populate", requiresUser, populateDB);
routes.get("/api/needPopulate", requiresUser, needPopulate);

// -----------------------------Gateways Routes-------------------------------------------

// Create
routes.post(
  "/api/gateways",
  [requiresUser, validateRequest(GatewaySchema.createGatewaySchema)],
  GatewayController.createGatewayFn
);

// Get
routes.get("/api/gateways/:_id", requiresUser, GatewayController.getGatewayFn);
routes.get("/api/gateways", requiresUser, GatewayController.getGatewaysFn);
routes.get(
  "/api/gatewaysWithDevices",
  GatewayController.getGatewaysWithDevicesFn
);

// Update
routes.put(
  "/api/gateways/:_id",  
  [requiresUser, validateRequest(GatewaySchema.updateGatewaySchema)],
  GatewayController.updateGatewayFn
);

// Delete
routes.delete(
  "/api/gateways/:_id",
  [requiresUser, validateRequest(GatewaySchema.deleteGatewaySchema)],
  GatewayController.deleteGatewayFn
);


// --------------------------------Devices Routes----------------------------------------

// Create
routes.post(
  "/api/Devices",
  [requiresUser, validateRequest(DeviceSchema.createDeviceSchema)],
  DeviceController.createDeviceFn
);

// Get
routes.get("/api/Devices/:_id", requiresUser, DeviceController.getDeviceFn);
routes.get("/api/Devices", requiresUser, DeviceController.getDevicesFn);

// Update
routes.put(
  "/api/Devices/:_id",
  [requiresUser, validateRequest(DeviceSchema.updateDeviceSchema)],
  DeviceController.updateDeviceFn
);

// Delete
routes.delete(
  "/api/Devices/:_id",
  [requiresUser, validateRequest(DeviceSchema.deleteDeviceSchema)],
  DeviceController.deleteDeviceFn
);

export default routes;
