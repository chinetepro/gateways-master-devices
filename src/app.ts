import config from "config";
import cors from "cors";
import express from "express";
import connect from "./database/connect";
import { errorHandler } from "./errors/error.handler";
import log from "./loggerPretty";
import { deserializeUser, errorMiddleware } from "./middleware";
import routes from "./routes/routes";

const port = config.get("port") as number;
const host = config.get("host") as string;

const app = express();
app.use(deserializeUser);

// parse requests of content-type - application/json
app.use(express.json());  /* bodyParser.json() is deprecated */

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: false }));   /* bodyParser.urlencoded() is deprecated */

// enable all CORS request
app.use(cors());

app.use("/", routes);

connect();

// @ts-ignore
app.use(errorMiddleware);

app.listen(port, host, () => {
  log.info(`Server running at http://${host}:${port}`);
});

process.on("uncaughtException", (error: Error) => {
  errorHandler.handleError(error);
  if (!errorHandler.isTrustedError(error)) {
    process.exit(1);
  }
});

process.on("unhandledRejection", (reason: Error, promise: Promise<any>) => {
  throw reason;
});

export default app;
