import { number, object, string } from "yup";
import { DeviceStatusEnum } from "../model/device.model";

const params = {
  params: object({
    _id: string().required(),
  }),
};

export const createDeviceSchema = object({
  body: object({
    uid: number(),
    vendor: string().required().max(50),
    createdAt: string(),
    status: string().test(
      "is status type",
      `body.status: valid device statuses: ${Object.values(
        DeviceStatusEnum
      )}`,
      (v) => {
        if (!v) return true;
        // @ts-ignore
        return v ? Object.values(DeviceStatusEnum).includes(v) : true;
      }
    ),
    gateway: string(),
  }),
});

export const updateDeviceSchema = object({
  ...params,
  body: object({
    uid: number(),
    vendor: string().max(50),
    createdAt: string(),
    status: string().test(
      "is status type",
      `body.status: valid device statuses: ${Object.values(
        DeviceStatusEnum
      )}`,
      (v) => {
        if (!v) return true;
        // @ts-ignore
        return v ? Object.values(DeviceStatusEnum).includes(v) : true;
      }
    ),
    gateway: string(),
  }),
});

export const deleteDeviceSchema = object({
  ...params,
});
