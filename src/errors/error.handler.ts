import log from "../loggerPretty";
import BaseError from "./base.error";

class ErrorHandler {
  
  public isTrustedError(error: Error) {
    if (error instanceof BaseError) {
      return error.isOperational;
    }
    return false;
  }

  public async handleError(err: Error): Promise<void> {
    await log.error(
      "Error message from the centralized error-handling component",
      err.stack
    );
  }
}
export const errorHandler = new ErrorHandler();
