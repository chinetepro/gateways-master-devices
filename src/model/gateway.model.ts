import mongoose from "mongoose";
import { UserDocument } from "./user.model";

export interface GatewayDocument extends mongoose.Document {
  userId: UserDocument["_id"];
  serialNumber: string;
  name: string;
  ipv4Address: string;
  createdAt: Date;
  updatedAt: Date;
}

const GatewaySchema = new mongoose.Schema(
  {
    userId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
    serialNumber: { type: String, unique: true, required: true },
    name: { type: String, required: true },
    ipv4Address: { type: String, required: true },
  },
  {
    timestamps: true,
  }
);

const Gateway = mongoose.model<GatewayDocument>("Gateway", GatewaySchema);

export default Gateway;
