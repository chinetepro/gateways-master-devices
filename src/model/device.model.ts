import mongoose, { Schema } from "mongoose";
import { customAlphabet } from "nanoid";
import { DBError } from "../errors";
import { GatewayDocument } from "./gateway.model";
import { UserDocument } from "./user.model";

const nanoid = customAlphabet("1234567890", 10);

export type DeviceStatus = "online" | "offline";
export enum DeviceStatusEnum {
  online = "online",
  reject = "offline",
}

export interface DeviceDocument extends mongoose.Document {
  userId: UserDocument["_id"];
  uid: number;
  vendor: string;
  status: DeviceStatus; // online/offline.
  createdAt: Date;
  gateway: GatewayDocument["_id"];
}

const DeviceSchema = new mongoose.Schema({
  userId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
  uid: {
    type: Number,
    required: true,
    unique: true,
    default: () => nanoid(),
  },
  vendor: { type: String, required: true },
  status: { type: String, default: "offline" as DeviceStatus },
  createdAt: { type: Date, default: new Date() },
  gateway: { type: Schema.Types.ObjectId, ref: "Gateway" },
});

// @ts-ignore
DeviceSchema.post("findOne", function (error, doc, next) {
  if (error) {
    next(new DBError());
  } else {
    next();
  }
});

const Device = mongoose.model<DeviceDocument>(
  "Device",
  DeviceSchema
);

export default Device;
