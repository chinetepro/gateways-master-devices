import {connect} from "mongoose";
import config from "config";
import log from "../loggerPretty";

const connectDatabase = () => {
  const mongoDbUrl = config.get("mongoDbUrl") as string;

  return connect(mongoDbUrl, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .then(() => {
      log.info("Database connected");
    })
    .catch((error) => {
      log.error("db error", error);
      process.exit(1);
    });
}

export default connectDatabase;
