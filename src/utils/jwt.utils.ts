import jwt from "jsonwebtoken";
import config from "config";

const privateKey = config.get("privateKey") as string;

if (!privateKey) {
  console.error('FATAL ERROR: privateKey is not defined.');
  process.exit(1);
}


export const signUser = (object: Object, options?: jwt.SignOptions | undefined) => {
  return jwt.sign(object, privateKey, options);
}

export const decodeToken = (token: string) => {
  try {
    const decoded = jwt.verify(token, privateKey);
    return { valid: true, expired: false, decoded };
  } catch (error : any) {
    return {
      valid: false,
      expired: error.message === "jwt expired",
      decoded: null,
    };
  }
}
