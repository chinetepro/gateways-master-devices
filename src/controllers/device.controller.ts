import { NextFunction, Request, Response } from "express";
import { get } from "lodash";
import { DBError } from "../errors";
import { findOneGateway } from "../service/gateway.service";
import {
  createDevice,
  deleteDevice,
  findAndUpdateDevice,
  findOneDevice,
  findDevice,
} from "../service/device.service";

const validateGateway = async (request: Request, response: Response) => {
  const body = request.body;

  const gatewayId = get(body, "gateway");

  if (gatewayId) {
    const gateway = await findOneGateway({ _id: gatewayId });
    if (gateway) {
      return gatewayId;
    }
  }
  return null;
}

const gatewayAllowAttachDevice = async (gatewayId: number) => {
  // Validate a gateway cannot have more than 10 devices
  const devices = await findDevice({ gateway: gatewayId });
  if (devices.length >= 10) return false;
  return true;
}

export const createDeviceFn = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const userId = get(request, "user._id");
    const body = request.body;

    let data = { ...body };

    const gatewayId = get(body, "gateway");
    if (gatewayId) {
      const gateway = await validateGateway(request, response);

      if (gateway) {
        const allow = await gatewayAllowAttachDevice(gatewayId);
        if (allow) {
          data = { ...body, gateway, userId };
        } else {
          return response
            .status(400)
            .send(
              "body.gateway: doesn't allow more than 10 devices attached"
            );
        }
      } else {
        return response.status(404).send("body.gateway: not found");
      }
    }

    const device = await createDevice(data);

    return response.send(device);
  } catch (err: any) {
    next(new DBError(err));
  }
}

export const updateDeviceFn = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const _id = get(request, "params._id");
    let update = request.body;

    const device = await findOneDevice({ _id });

    if (!device) {
      return response.sendStatus(404);
    }

    const aux = get(update, "gateway");
    if (aux === "detach") {
      update = { ...update, gateway: null };
    } else if (aux) {
      const gateway = await validateGateway(request, response);
      if (gateway) {
        const allow = await gatewayAllowAttachDevice(aux);
        if (allow) {
          update = { ...update, gateway };
        } else {
          return response
            .status(400)
            .send(
              "body.gateway: doesn't allow more than 10 devices attached"
            );
        }
      } else {
        return response.status(404).send("body.gateway: not found");
      }
    }

    const updatedDevice = await findAndUpdateDevice({ _id }, update, {
      new: true,
    });

    return response.send(updatedDevice);
  } catch (err: any) {
    next(new DBError(err));
  }
}

export const getDeviceFn = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const _id = get(request, "params._id");
    const device = await findOneDevice({ _id });

    if (!device) {
      return response.sendStatus(404);
    }

    return response.send(device);
  } catch (err: any) {
    next(new DBError(err));
  }
}

export const getDevicesFn = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const devices = await findDevice({});

    return response.send(devices);
  } catch (err: any) {
    next(new DBError(err));
  }
}

export const deleteDeviceFn = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const _id = get(request, "params._id");

    const device = await findOneDevice({ _id });

    if (!device) {
      return response.sendStatus(404);
    }

    await deleteDevice({ _id });

    return response.sendStatus(200);
  } catch (err: any) {
    next(new DBError(err));
  }
}
