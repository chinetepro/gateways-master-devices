import { NextFunction, Request, Response } from "express";
import mongoose from "mongoose";
import { get } from "lodash";
import { DBError } from "../errors";
import Device from "../model/device.model";
import {
  createGateway,
  deleteGateway,
  findAndUpdateGateway,
  findOneGateway,
  findGateway,
  findGatewaysWithDevices,
} from "../service/gateway.service";

export const createGatewayFn = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const userId = get(request, "user._id");
    const body = request.body;

    const gateway = await createGateway({ ...body, userId });

    return response.send(gateway);
  } catch (err: any) {
    next(new DBError(err));
  }
}

export const updateGatewayFn = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const _id = get(request, "params._id");
    const update = request.body;

    const gateway = await findOneGateway({ _id });

    if (!gateway) {
      return response.sendStatus(404);
    }

    const updatedGateway = await findAndUpdateGateway({ _id }, update, {
      new: true,
    });

    return response.send(updatedGateway);
  } catch (err: any) {
    next(new DBError(err));
  }
}

export const getGatewayFn = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const _id = get(request, "params._id");

    const gateway = await findOneGateway({ _id });

    if (!gateway) {
      return response.sendStatus(404);
    }

    return response.send(gateway);
  } catch (err: any) {
    next(new DBError(err));
  }
}

export const getGatewaysFn = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const userId = get(request, "user._id");
    const gateways = await findGateway({ userId });

    return response.send(gateways);
  } catch (err: any) {
    next(new DBError(err));
  }
}

export const getGatewaysWithDevicesFn = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const userId = get(request, "user._id");
    const gateways = await findGatewaysWithDevices({userId});

    return response.send(gateways);
  } catch (err: any) {
    next(new DBError(err));
  }
}

export const deleteGatewayFn = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  try {
    const _id = get(request, "params._id");

    const gateway = await findOneGateway({ _id });

    if (!gateway) {
      return response.sendStatus(404);
    }

    const session = await mongoose.startSession();
    await session.withTransaction(async () => {
      await deleteGateway({ _id });
      await Device.updateMany({ gateway: _id }, { gateway: null });
    });
    session.endSession();

    return response.sendStatus(200);
  } catch (err: any) {
    next(new DBError(err));
  }
}
