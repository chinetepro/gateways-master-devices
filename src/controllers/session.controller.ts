import config from "config";
import { get } from "lodash";
import { Request, Response } from "express";
import { findUser, validatePassword } from "../service/user.service";
import {
  createSession,
  getAccessToken,
  updateSession,
  findSessions,
} from "../service/session.service";
import { signUser } from "../utils/jwt.utils";

export async function createUserSessionFn(request: Request, response: Response) {
  // validate the email and password
  const user = await validatePassword(request.body);

  if (!user) {
    return response.status(401).send("Invalid username or password");
  }

  // Create a session
  const session = await createSession(user._id, request.get("user-agent") || "");

  // create access token
  const accessToken = getAccessToken({
    user,
    session,
  });

  // create refresh token
  const refreshToken = signUser(session, {
    expiresIn: config.get("refreshTokenTime"), // 1 year
  });

  // send refresh & access token back
  return response.send({ accessToken, refreshToken, user });
}

export async function invalidateUserSessionFn(
  request: Request,
  response: Response
) {
  const sessionId = get(request, "user.session");

  await updateSession({ _id: sessionId }, { valid: false });

  return response.sendStatus(200);
}

export async function getUserSessionsFn(request: Request, response: Response) {
  const userId = get(request, "user._id");

  const sessions = await findSessions({ user: userId, valid: true });
  const user = await findUser({ _id: userId });

  return response.send({sessions , user});
}
