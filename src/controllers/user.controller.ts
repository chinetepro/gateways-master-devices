import { Request, Response } from "express";
import { omit } from "lodash";
import { createUser } from "../service/user.service";
import log from "../loggerPretty";

export const createUserFn = async (request: Request, response: Response) => {
  try {
    const user = await createUser(request.body);
    return response.send(omit(user.toJSON(), "password"));
  } catch (e : any) {
    log.error(e);
    return response.status(409).send(e.message);
  }
}
