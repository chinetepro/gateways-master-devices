import { NextFunction, Request, Response } from "express";
import { DBError } from "../errors";
import { get } from "lodash";
import Gateway from "../model/gateway.model";
import Device from "../model/device.model";
import { gateways } from "./gateways.seed";
import { devices } from "./devices.seed";

const mappedArrayWithUser = (data: Array<any>, userId: string) => data.map((item: any) => ({...item , userId}));


const getRequestPopulate = async (userId: string) => {
  const [g, p] = await Promise.all([Gateway.findOne({ userId }).exec(), Device.findOne({ userId }).exec()]);
  return { g, p };
};

export async function populateDB(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const userId = get(req, "user._id");
    const mappedGateways = mappedArrayWithUser(gateways, userId);
    const mappedDevices = mappedArrayWithUser(devices, userId);
    const {g, p} = await getRequestPopulate(userId);
    if (!g && !p) {
      await Gateway.insertMany(mappedGateways);
      await Device.insertMany(mappedDevices);
    }

    return res.sendStatus(200);
  } catch (err: any) {
    next(new DBError(err));
  }
}

export async function needPopulate(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const userId = get(req, "user._id");
    const {g, p} = await getRequestPopulate(userId);
    if (!g && !p) {
      return res.status(200).send("Yes");
    }

    return res.status(200).send("No");
  } catch (err: any) {
    next(new DBError(err));
  }
}
