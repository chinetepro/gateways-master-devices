import {
  DocumentDefinition,
  FilterQuery,
  QueryOptions,
  UpdateQuery,
} from "mongoose";
import { DBError } from "../errors";
import Device, { DeviceDocument } from "../model/device.model";

export function createDevice(
  input: DocumentDefinition<DeviceDocument>
) {
  return Device.create(input);
}

export function findDevice(
  query: FilterQuery<DeviceDocument>,
  options: QueryOptions = { lean: true }
) {
  return Device.find(query, {}, options).exec();
}

export function findOneDevice(
  query: FilterQuery<DeviceDocument>,
  options: QueryOptions = { lean: true }
) {
  return Device.findOne(query, {}, options, (err) => {
    if (err) {
      throw new DBError();
    }
  });
}

export function findAndUpdateDevice(
  query: FilterQuery<DeviceDocument>,
  update: UpdateQuery<DeviceDocument>,
  options: QueryOptions
) {
  return Device.findOneAndUpdate(query, update, options);
}

export function deleteDevice(query: FilterQuery<DeviceDocument>) {
  return Device.deleteOne(query);
}
