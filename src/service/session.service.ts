import { LeanDocument, FilterQuery, UpdateQuery } from "mongoose";
import config from "config";
import { get } from "lodash";
import { UserDocument } from "../model/user.model";
import Session, { SessionDocument } from "../model/session.model";
import { signUser, decodeToken } from "../utils/jwt.utils";
import { findUser } from "./user.service";

export const createSession = async (userId: string, userAgent: string) => {
  const session = await Session.create({ user: userId, userAgent });
  return session.toJSON();
}

export const getAccessToken = ({
  user,
  session,
}: {
  user:
    | Omit<UserDocument, "password">
    | LeanDocument<Omit<UserDocument, "password">>;
  session:
    | Omit<SessionDocument, "password">
    | LeanDocument<Omit<SessionDocument, "password">>;
}) => {
  // Build and return the new access token
  const accessToken = signUser(
    { ...user, session: session._id },
    { expiresIn: config.get("accessTokenTime") } // 30 minutes
  );

  return accessToken;
}

export const getNewAccessToken = async ({
  refreshToken,
}: {
  refreshToken: string;
}) => {
  // Decode the refresh token
  const { decoded } = decodeToken(refreshToken);

  if (!decoded || !get(decoded, "_id")) return false;

  // Get the session
  const session = await Session.findById(get(decoded, "_id"));

  // Make sure the session is still valid
  if (!session || !session?.valid) return false;

  const user = await findUser({ _id: session.user });

  if (!user) return false;

  const accessToken = getAccessToken({ user, session });

  return accessToken;
}

export const updateSession = async (
  query: FilterQuery<SessionDocument>,
  update: UpdateQuery<SessionDocument>
) => {
  return Session.updateOne(query, update);
}

export const findSessions = async (query: FilterQuery<SessionDocument>) =>{
  return Session.find(query).lean();
}
